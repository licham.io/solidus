# Pin npm packages by running ./bin/importmap

pin "application", preload: true

# Stimulus & Turbo
pin "@hotwired/stimulus", to: "stimulus.js"
pin "@hotwired/stimulus-loading", to: "stimulus-loading.js"
pin "@hotwired/turbo-rails", to: "turbo.js"

pin_all_from "app/javascript/controllers", under: "controllers"