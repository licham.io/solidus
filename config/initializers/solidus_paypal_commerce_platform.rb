# frozen_string_literal: true

SolidusPaypalCommercePlatform.configure do |config|
  # TODO: Remember to change this with the actual preferences you have implemented!
  # config.sample_preference = 'sample_value'
end

Spree.config do |config|
  config.static_model_preferences.add(
    'SolidusPaypalCommercePlatform::PaymentMethod',
    'Paypal Credentials Storage', {
      test_mode: !Rails.env.production?,
      client_id: ENV['PAYPAL_CLIENT_ID'],
      client_secret: ENV['PAYPAL_CLIENT_SECRET'],
      display_credit_messaging: false,
      display_on_product_page: true,
      display_on_cart: true,
      paypal_button_color: 'blue',
      venmo_standalone: 'enabled'
    }
  )
end

# Given the nature of our company and products we're very opposed
# to enabling paypal's credit features. Patch them out.

# This is a pretty nasty hack, and will need to be checked during each upgrade
# of the paypal gem.
Rails.application.config.after_initialize do
  SolidusPaypalCommercePlatform::PaymentMethod.class_eval do
    def javascript_sdk_url(order: nil, currency: nil)
      # Ref: https://developer.paypal.com/sdk/js/configuration/

      # Both instance and class respond to checkout_steps.
      step_names = order ? order.checkout_steps : ::Spree::Order.checkout_steps.keys

      commit_immediately = step_names.include? "confirm"

      parameters = {
        'client-id': client_id,
        intent: auto_capture? ? "capture" : "authorize",
        commit: commit_immediately ? "false" : "true",
        components: options[:display_credit_messaging] ? "buttons,messages" : "buttons",
        currency: currency,
      }

      parameters['enable-funding'] = 'venmo' if venmo_standalone_enabled?

      # This line is the only new code we've added.
      parameters['disable-funding'] = 'paylater'

      unless Rails.env.production?
        parameters['buyer-country'] = options[:force_buyer_country].presence
      end

      "https://www.paypal.com/sdk/js?#{parameters.compact.to_query}".html_safe # rubocop:disable Rails/OutputSafety
    end
  end
end