#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../config/environment'

def prompt(prompt, default, secret = false)
  return default if ENV['BOOTSTRAP_NO_PROMPT']

  STDERR.printf prompt
  input = secret ? STDIN.noecho(&:gets).chomp : STDIN.gets.chomp
  puts

  input
end

def create_user(email)
  return if Spree::User.find_by(email: email)

  puts "Creating user: #{email}"
  password = prompt("#{email} Password: ", "test123", true)

  user = Spree::User.new(
    email: email,
    login: email,
    password: password,
    password_confirmation: password,
  )

  if user.save
    user.spree_roles << $admin_role
    user.save
    user.generate_spree_api_key!
  else
    warn "Unable to create user: #{email}"
    warn(user.errors.full_messages.map { |m| "- #{m}" })
    warn "(attributes: #{user.attributes.inspect})"
  end
end

def create_taxon(name, taxonomy)
  return if Spree::Taxon.find_by(name: name)

  puts "Creating Taxon: #{name}"
  Spree::Taxon.create(
    name: name,
    taxonomy: taxonomy,
    parent: taxonomy.root
  )
end

def create_flat_rate_shipping_method(name, admin_name, carrier, rate, zones, category_name)
  category = Spree::ShippingCategory.find_or_create_by(name: category_name)
  method = Spree::ShippingMethod.find_or_create_by(name: name, admin_name: admin_name)
  method.admin_name = admin_name
  method.calculator = Spree::Calculator::FlatRate.create!(preferred_amount: rate)
  method.shipping_categories = [category]
  method.zones = zones
  method.available_to_users = true
  method.available_to_all = true
  method.save!

  method
end

puts "Creating admin role"
$admin_role = Spree::Role.find_or_create_by(name: 'admin')

puts "Setting up users"
create_user("sysadmin@acivildawn.com")
create_user("directors@acivildawn.com")

puts "Deleting default admin user"
Spree::User.delete_by(email: "admin@example.com")

puts "Creating taxonomies and taxons"
taxonomy = Spree::Taxonomy.find_or_create_by(name: "Categories")
create_taxon("Prints", taxonomy)
create_taxon("Stickers", taxonomy)

puts "Configuring store details, get ready for lots of prompts..."
store = Spree::Store.default
begin store.name = prompt("Store Name: ", "Local Development Store")
store.code = prompt("Store Slug: ", "local-development-store")
store.url = prompt("Store URL: ", "http://localhost:3000")
store.mail_from_address = prompt("Store Mail From Address: ", "Local Development Orders <orders@acivildawn.com>")
store.bcc_email = prompt("Store BCC Email: ", "orders@acivildawn.com")
store.meta_keywords = prompt("Store Meta Keywords: ", "development")
store.meta_description = prompt("Store Welcome Message: ", "Greetings, welcome to the local dev instance")
store.default_currency = "USD"
store.cart_tax_country_iso = "US"
store.available_locales = ["en"]
store.save!
end

puts "Making store credit only available to admins"
credit = Spree::PaymentMethod.find_or_create_by(name: "Store Credit", type: "Spree::PaymentMethod::StoreCredit")
credit.available_to_users = false
credit.available_to_admin = true
credit.save!

puts "Creating Paypal payment method"
paypal = Spree::PaymentMethod.find_or_create_by(name: "Pay with Paypal", type: "SolidusPaypalCommercePlatform::PaymentMethod")
paypal.preference_source = "Paypal Credentials Storage"
paypal.available_to_users = true
paypal.available_to_admin = true
paypal.save!

puts "Enabling payment methods for store"
store.payment_methods = [credit, paypal]
store.save!

puts "Setting up shipping zones"
Spree::Zone.destroy_all
zones = {}
zones["US"] = Spree::Zone.find_or_create_by(name: "United States")
zones["US"].description = "USA"
zones["US"].countries = [Spree::Country.find_by(iso: "US")]
zones["US"].save!
zones["EU_VAT"] = Spree::Zone.find_or_create_by(name: "EU_VAT")
zones["EU_VAT"].description = "Countries that make up the EU VAT zone."
zones["EU_VAT"].countries = ["AT", "BE", "BG", "CY", "CZ", "DE", "DK", "EE", "ES", "FI", "FR", "GB", "GR", "HR", "HU", "IE", "IT", "LT", "LU", "LV", "MT", "NL", "PL", "PT", "RO", "SE", "SI", "SK"].map { |iso| Spree::Country.find_by(iso: iso) }
zones["EU_VAT"].save!
zones["Worldwide"] = Spree::Zone.find_or_create_by(name: "Worldwide")
zones["Worldwide"].description = "International Shipping"
zones["Worldwide"].countries = Spree::Country.all - (zones["US"].countries + zones["EU_VAT"].countries)
zones["Worldwide"].save!

puts "Setting up shipping methods"
Spree::ShippingMethod.destroy_all
store.shipping_methods << create_flat_rate_shipping_method("Flat Rate Shipping (Prints)", "Domestic Flat Rate Shipping (Prints)", "USPS", 2.00, [zones["US"]], "Prints")
store.shipping_methods << create_flat_rate_shipping_method("Flat Rate Shipping (Stickers)", "Domestic Flat Rate Shipping (Stickers)", "USPS", 1.00, [zones["US"]], "Stickers")
store.shipping_methods << create_flat_rate_shipping_method("Flat Rate Shipping (Prints)", "International Flat Rate Shipping (Prints)", "USPS", 6.00, [zones["EU_VAT"], zones["Worldwide"]], "Prints")
store.shipping_methods << create_flat_rate_shipping_method("Flat Rate Shipping (Stickers)", "International Flat Rate Shipping (Stickers)", "USPS", 4.00, [zones["EU_VAT"], zones["Worldwide"]], "Stickers")
store.save!

puts "Done!"
