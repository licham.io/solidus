#!/bin/bash

VERSION=$1

if [[ -z $VERSION ]]; then
	echo "Must specify version to create" >&2
fi

git checkout main
sed -i "s/DOCKER_IMAGE_TAG: \".*\"/DOCKER_IMAGE_TAG: \"$VERSION\"/" .gitlab-ci.yml
sed -i "s/tag: \".*\"/tag: \"$VERSION\"/" k8s/solidus/values.yaml
git add .gitlab-ci.yml k8s/solidus/values.yaml
git commit -m "Release: $VERSION"
git tag $VERSION $(git log -n1 --pretty='format:%h') -m "Release: $VERSION"
git push origin HEAD --tags
