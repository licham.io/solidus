#!/bin/bash

bundle exec rake db:reset
BOOTSTRAP_NO_PROMPT=true bundle exec ./bin/bootstrap-database.rb

echo "Admin user created with email sysadmin@acivildawn.com and password test123" >&1
echo "To load a bunch of fake products for testing, run 'bundle exec rake spree_sample:load'" >&1
echo "Run './bin/rails server' to start rails" >&1