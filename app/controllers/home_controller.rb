# frozen_string_literal: true

class HomeController < StoreController
  helper 'spree/products'
  respond_to :html

  def index
    @categories = Spree::Taxon.where("permalink LIKE ?", "categories/%").where(depth: 1).order(:lft)
    @searcher = build_searcher(params.merge(include_images: true))
    @products = @searcher.retrieve_products.shuffle

    @displayed_products = {}
    @displayed_products[:featured] = @products.select { |p| p.property("Featured") == "true" }.first(3)

    @categories.each do |category|
      @displayed_products[category.name.to_sym] = (@products - @displayed_products.flatten).select { |p| p.taxons.include? category }.first(2)
    end

    @displayed_products[:highlighted] = (@products - @displayed_products.values.flatten).first
  end
end
