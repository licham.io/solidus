module TaxonsHelper
  def taxon_path(taxon)
    "t/#{taxon.permalink}"
  end

  def taxon_seo_url(taxon)
    nested_taxons_path(taxon.permalink)
  end
end
